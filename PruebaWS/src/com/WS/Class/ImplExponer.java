package com.WS.Class;

import javax.jws.WebService;

@WebService(endpointInterface = "com.WS.Class.ExponerWS")
public class ImplExponer implements ExponerWS {

	@Override
	public double calcular(int opcion, int parametro1, int paramatro2) {
	
		double resultado=0;
		
		switch (opcion) {
		case 1:
			System.out.println("Operacion suma");
			resultado=parametro1+paramatro2;
			break;
		case 2:
			System.out.println("Operacion Resta");
			resultado=parametro1-paramatro2;
			break;
		case 3:
			System.out.println("Operacion Multiplicacion");
			resultado=parametro1*paramatro2;
			break;
		case 4:
			System.out.println("Operacion Division");
			resultado=parametro1/paramatro2;
			break;

		default:
			break;
		}
	 return resultado;
	}

}
