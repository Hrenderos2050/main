/**
 * ExponerWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.WS.Class;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ExponerWS extends java.rmi.Remote {
	@WebMethod
    public double calcular(int arg0, int arg1, int arg2) ;
}
